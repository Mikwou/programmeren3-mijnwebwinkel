﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LerenWerkenMetMVC.Models;

namespace LerenWerkenMetMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult AboutMe()
        {
            ViewBag.Message = "De About Me pagina";


            return View();
        }

        public ActionResult AboutMeMetModel()
        {
            ViewBag.Message = "De About Me met Model";
            LerenWerkenMetMVC.Models.Persoon persoon = new LerenWerkenMetMVC.Models.Persoon();
            persoon.Voornaam = "Mike";
            persoon.Familienaam = "Wouters";
            persoon.Rootpath = Server.MapPath("~");
            persoon.FotoURL = "../../Content/Images/FotoKinesist.jpg";
            persoon.Biografie = "/Content/Data/Biografie.html";
            return View(persoon);

        }

        public ActionResult Postcodes()
        {
            ViewBag.Message = "Postcodes";

            Postcode postcodes = new Postcode();
            List<Postcode> mijnPostCodesLijst = postcodes.GetList();
            return View(mijnPostCodesLijst);
        }
    }
}