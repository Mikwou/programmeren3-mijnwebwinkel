﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LerenWerkenMetMVC.Models
{
    public class Postcode
    {
        private string code;

        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        private string plaats;

        public string Plaats
        {
            get { return plaats; }
            set { plaats = value; }
        }

        private string provincie;

        public string Provincie
        {
            get { return provincie; }
            set { provincie = value; }
        }

        private string localité;

        public string Localité
        {
            get { return localité; }
            set { localité = value; }
        }

        private string province;

        public string Province
        {
            get { return province; }
            set { province = value; }
        }

        private string padNaarCSV;

        public string PadNaarCSV
        {
            get { return @"C:\Users\eMenM\Documents\Visual Studio 2015\Projects\Programmeren 3\LerenWerkenMetMVC\LerenWerkenMetMVC\App_Data\postcodes.csv"; }
        }

        public List<Postcode> list;

        public string LeesUitCsvBestand()
        {
            Dal.Tekstbestand bestand = new Dal.Tekstbestand();
            LerenWerkenMetMVC.Models.Postcode locatieNaarBestand = new LerenWerkenMetMVC.Models.Postcode();
            bestand.FileName = locatieNaarBestand.PadNaarCSV;
            bestand.Lees();
            return bestand.Text;
        }

        public List<Postcode> GetList()
        {

            string[] postcodes = this.LeesUitCsvBestand().Split('\n');
            list = new List<Postcode>();
            foreach (string s in postcodes)
            {
                list.Add(this.ToObject(s));
            }
            list.RemoveAt(0); //verwijder de hoofding
            list.RemoveAt(list.Count - 1); // verwijder de laatste lege lijn
            return list;
        }

        public Postcode ToObject(string line)
        {
            if (line != "")
            {
                Postcode postcode = new Postcode();
                string[] values = line.Split(',');
                postcode.code = values[0];
                postcode.Plaats = values[1];
                postcode.Provincie = values[2];
                postcode.Localité = values[3];
                postcode.Province = values[4];
                return postcode;
            }
            else
            {
                return null;
            }
        }

    }
}